<?php
session_start();

global $modules;
$modules = get_loaded_extensions();

global $isGood;
$isGood = true;

$archive = "deltadoc.zip";

if (!file_exists($archive)) {
    $isGood = false;
}

if (isset($_GET["key"])) {
    $res = array('status' => '', 'message' => "");
    $host = $_GET["host"];
    $db = $_GET["db"];
    $username = $_GET["username"];
    $password = $_GET["password"];
    $driver = $_GET["driver"];
    switch ($_GET["key"]) {
        case 'tflex':
            $configTFlex = array();
            $configTFlex["host"] = $host;
            $configTFlex["db"] = $db;
            $configTFlex["username"] = $username;
            $configTFlex["password"] = $password;
            $_SESSION['config_db_tflex'] = json_encode($configTFlex);
            try {
                @$dbh = new PDO("pgsql:host=$host;dbname=$db", $username, $password);
                $dbh = null;
                $res['status'] = 'success';
                $res["message"] = $_SESSION['config_db_tflex'];
            } catch (Exception $e) {
                $_SESSION['config_db_tflex'] = "";
                $res['status'] = 'error';
                $res['message'] = $e->getMessage();
            }
            break;
        case 'ascon':
            $configAscon = array();
            $configAscon["host"] = $host;
            $configAscon["db"] = $db;
            $configAscon["username"] = $username;
            $configAscon["password"] = $password;
            $_SESSION['config_db_ascon'] = json_encode($configAscon);
            try {
                $connectionInfo = array("Database" => $configAscon["db"], "UID" => $configAscon["username"], "PWD" => $configAscon["password"]);
                $conn = sqlsrv_connect($configAscon["host"], $connectionInfo);
                if ($conn === false) {
                    $res['status'] = 'error';
                    $res["message"] = sqlsrv_errors();
                } else {
                    $res['status'] = 'success';
                    $res["message"] = $_SESSION['config_db_tflex'];
                }
            } catch (Exception $e) {
                $_SESSION['config_db_tflex'] = "";
                $res['status'] = 'error';
                $res['message'] = $e->getMessage() . "\n" . $_GET;
            }
            break;
        case 'zip':
            if ($isGood) {
                set_time_limit(600);
                $zip = new ZipArchive;
                if ($zip->open($archive) === TRUE) {
                    $zip->extractTo(dirname(__FILE__));
                    $zip->close();
                    $res['status'] = 'success';
                    $res['message'] = "extractTo";
                } else {
                    $res['status'] = 'error';
                    $res['message'] = "";
                }
            } else {
                $res['status'] = 'error';
                $res['message'] = "Файл $archive не найден";
            }
            break;
        case 'create_config_db':
            if ($isGood) {

                $configDbTflex = json_decode($_SESSION['config_db_tflex'], true);
                $configDbAscon = json_decode($_SESSION['config_db_ascon'], true);


                $contentFile = "";
                $contentFile .= "<?php ";
                $contentFile .= "return [";
                $contentFile .= "'TFlexDocs_Start' => [";
                $contentFile .= "'class' => 'yii\db\Connection',";
                $contentFile .= "'dsn' => 'pgsql:host=" . $configDbTflex['host'] . ";port=5432;dbname=" . $configDbTflex['db'] . "',";
                $contentFile .= "'username' => '" . $configDbTflex['username'] . "',";
                $contentFile .= "'password' => '" . $configDbTflex['password'] . "',";
                $contentFile .= "'charset' => 'utf-8',";
                $contentFile .= "'schemaMap' => ['pgsql' => ['class' => 'yii\db\pgsql\Schema','defaultSchema' => 'dbo'],";
                $contentFile .= "],";
                $contentFile .= "],";
                $contentFile .= "'ST3D5' => [";
                $contentFile .= "'class' => 'yii\db\Connection',";
                $contentFile .= "'driverName' => 'sqlsrc',";
                $contentFile .= "'dsn' => 'sqlsrv:Server=" . $configDbAscon['host'] . ";Database=" . $configDbAscon['db'] . "',";
                $contentFile .= "'host' => '" . $configDbAscon['host'] . "',";
                $contentFile .= "'database' => '" . $configDbAscon['db'] . "',";
                $contentFile .= "'username' => '" . $configDbAscon['username'] . "',";
                $contentFile .= "'password' => '" . $configDbAscon['password'] . "',";
                $contentFile .= "'charset' => 'utf-8',";
                $contentFile .= "],";
                $contentFile .= "];";
                $contentFile .= "?>";
                $fp = fopen("config/db.php", "w+");
                fwrite($fp, $contentFile);
                fclose($fp);
                $res['status'] = 'success';
                $res['message'] = "create_file";
            }
            break;
        default:
            $res['status'] = 'error';
            $res['message'] = 'Неизвестный ключ';
            $res['get'] = json_encode($_GET);
            break;
    }
    print json_encode($res);
    die;
    exit();
}
// T-Flex DOCs
$configDatabaseTflex = array();
$configDatabaseTflex["host"] = "";
$configDatabaseTflex["db"] = "";
$configDatabaseTflex["username"] = "";
$configDatabaseTflex["password"] = "";
if (isset($_SESSION['config_db_tflex'])) {
    $configDatabaseTflex = json_decode($_SESSION['config_db_tflex'], true);
}
// ASCON
$configDatabaseAscon = array();
$configDatabaseAscon["host"] = "";
$configDatabaseAscon["db"] = "";
$configDatabaseAscon["username"] = "";
$configDatabaseAscon["password"] = "";
if (isset($_SESSION['config_db_ascon'])) {
    $configAscon = json_decode($_SESSION['config_db_ascon'], true);
}



function checkModule($name)
{
    global $modules;
    global $isGood;
    foreach ($modules as $value) {
        if (strtoupper($value) == strtoupper($name)) {
            return true;
        }
    }
    $isGood = false;
    return false;
}


function cssStatus($isStatus)
{
    if ($isStatus) {
        return "server-info-item-yes";
    } else {
        return "server-info-item-no";
    }
}

function cssStatusSvg($isStatus)
{
    if ($isStatus) {
        return '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-circle-fill" viewBox="0 0 16 16"><path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/></svg>';
    } else {
        return '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-circle-fill" viewBox="0 0 16 16"><path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/></svg>';
    }
}


function checkVer()
{
    global $isGood;
    $version = explode('.', phpversion());
    $version = $version[0] * 10000 + $version[1] * 100 + $version[2];
    if (intval($version) >= 50400) {
        return true;
    } else {
        $isGood = false;
        return false;
    }
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="." type="image/x-icon">
    <title>Установка ДельтаДок</title>
</head>

<body>
    <div class="container">
        <div class="card">
            <div class="card-title">
                <h1>Установка ДельтаДок</h1>
            </div>
            <!-- STEP 1 -->
            <div id="Step1" class="hidden">
                <div class="group-file-zip">
                    <?php if (file_exists($archive)) : ?>
                        <input class="form-control form-control-yes" disabled type="text" value="<?= $archive ?>">
                    <?php else : ?>
                        <input class="form-control form-control-no" disabled type="text" value="Файл не найден">
                    <?php endif ?>
                </div>
                <form id="FormTflex" action="." class="form-check d-flex flex-column">
                    <input type="hidden" name="key" value="tflex">
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-row">
                            <span class="form-label">Драйвер подключения</span>
                            <span class="form-control">pgsql (pdo_pgsql)</span>
                        </div>
                        <div class="d-flex">
                            <label class="d-flex flex-grow-1">
                                <span class="form-label">Адрес базы данных</span>
                                <input type="text" class="form-control" name="host" placeholder="localhost" value="<?= $configDatabaseTflex["host"] ?>">
                            </label>
                        </div>
                        <div class="d-flex">
                            <label class="d-flex flex-grow-1">
                                <span class="form-label">Наименование базы данных</span>
                                <input type="text" class="form-control" name="db" value="<?= $configDatabaseTflex["db"] ?>">
                            </label>
                        </div>
                        <div class="d-flex">
                            <label class="d-flex flex-grow-1">
                                <span class="form-label">Имя пользователя</span>
                                <input type="text" class="form-control" name="username" value="<?= $configDatabaseTflex["username"] ?>">
                            </label>
                        </div>
                        <div class="d-flex">
                            <label class="d-flex flex-grow-1">
                                <span class="form-label">Пароль</span>
                                <input type="password" class="form-control" name="password" autocomplete value="<?= $configDatabaseTflex["password"] ?>">
                            </label>
                        </div>
                        <div class="status d-flex flex-row">
                            <div class="status-text status-text-error">
                                <input class="form-check-input" type="checkbox" onclick="return false;">
                                <span class="status-text-text">Соединение не установлено</span>
                            </div>
                            <button type="button" class="btn" onclick="checkConnect('FormTflex')">Проверить</button>
                            <button type="reset" class="btn" onclick="formReset('FormTflex')">Сбросить</button>
                        </div>
                    </div>
                </form>
                <form id="FormAscon" action="." class="form-check d-flex flex-column">
                    <input type="hidden" name="key" value="ascon">
                    <div class="d-flex flex-column">
                        <div class="d-flex">
                            <div class="d-flex flex-row flex-grow-1">
                                <span class="form-label">Драйвер подключения</span>
                                <span class="form-control">sqlsrv</span>
                            </div>
                        </div>
                        <div class="d-flex">
                            <label class="d-flex flex-grow-1">
                                <span class="form-label">Адрес базы данных</span>
                                <input type="text" class="form-control" name="host" placeholder="localhost" value="<?= $configAscon["host"] ?>">
                            </label>
                        </div>
                        <div class="d-flex">
                            <label class="d-flex flex-grow-1">
                                <span class="form-label">Наименование базы данных</span>
                                <input type="text" class="form-control" name="db" value="<?= $configAscon["db"] ?>">
                            </label>
                        </div>
                        <div class="d-flex">
                            <label class="d-flex flex-grow-1">
                                <span class="form-label">Имя пользователя</span>
                                <input type="text" class="form-control" name="username" value="<?= $configAscon["username"] ?>">
                            </label>
                        </div>
                        <div class="d-flex">
                            <label class="d-flex flex-grow-1">
                                <span class="form-label">Пароль</span>
                                <input type="password" class="form-control" name="password" autocomplete value="<?= $configAscon["password"] ?>">
                            </label>
                        </div>
                        <div class="status d-flex">
                            <div class="status-text status-text-error">
                                <input class="form-check-input" type="checkbox" onclick="return false;">
                                <span class="status-text-text">Соединение не установлено</span>
                            </div>
                            <button type="button" class="btn" onclick="checkConnect('FormAscon')">Проверить</button>
                            <button type="reset" class="btn" onclick="formReset('FormAscon')">Сбросить</button>
                        </div>
                    </div>
                </form>
                <div class="server-info">
                    <div class="d-flex flex-column">
                        <?php if (checkVer()) : ?>
                            <div class="server-info-item <?= cssStatus(true) ?>">
                                <div class="server-info-module">Версия PHP <?= phpversion() ?></div>
                                <div class="server-info-status"><?= cssStatusSvg(true) ?></div>
                            </div>
                        <?php else : ?>
                            <div class="server-info-item <?= cssStatus(false) ?>">
                                <div class="server-info-module">
                                    <span>Версия PHP </span>
                                    <span><?= phpversion() ?></span>
                                    <span> (для корректной работы необходима версия 5.4 и новее)</span>
                                </div>
                                <div class="server-info-status"><?= cssStatusSvg(true) ?></div>
                            </div>
                        <?php endif ?>
                        <div class="server-info-item <?= cssStatus(checkModule('PDO')) ?>">
                            <div class="server-info-module">Модуль PDO для PHP</div>
                            <div class="server-info-status"><?= cssStatusSvg(checkModule('PDO')) ?></div>
                        </div>
                        <div class="server-info-item <?= cssStatus(checkModule('pdo_pgsql')) ?>">
                            <div class="server-info-module">Модуль pdo_pgsql для PHP</div>
                            <div class="server-info-status"><?= cssStatusSvg(checkModule('pdo_pgsql')) ?></div>
                        </div>
                        <div class="server-info-item <?= cssStatus(checkModule('sqlsrv')) ?>">
                            <div class="server-info-module">Модуль sqlsrv для PHP</div>
                            <div class="server-info-status"><?= cssStatusSvg(checkModule('sqlsrv')) ?></div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <?php if ($isGood) : ?>
                        <button class="btn btn-lg m-1" onclick="next()">Далее</button>
                    <?php else : ?>
                        <button class="btn btn-lg m-1 disabled">Далее</button>
                    <?php endif ?>
                </div>
            </div>
            <!-- STEP 2 -->
            <div id="Step2" class="hidden">
                <div class="d-flex flex-column">
                    <div class="d-flex ">
                        <h2 class="flex-grow-1">Распаковка архива</h2>
                        <div class="d-flex justify-content-center align-items-center">
                            <?php if (file_exists($archive)) : ?>
                                <input class="form-control form-control-yes" disabled type="text" value="<?= $archive ?>">
                            <?php else : ?>
                                <input class="form-control form-control-no" disabled type="text" value="Файл не найден">
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="load">
                        <div class="load-bar"></div>
                    </div>
                </div>
            </div>
            <!-- STEP 3 -->
            <div id="Step3" class="hidden">
                <div class="d-flex flex-column">
                    <div class="d-flex ">
                        <h2 class="flex-grow-1">Конфигурация БД</h2>
                        <div class="d-flex justify-content-center align-items-center">
                            <input class="form-control form-control-yes" disabled type="text" value="config/db.php">
                        </div>
                    </div>
                    <div class="load">
                        <div class="load-bar"></div>
                    </div>
                </div>
            </div>
            <!-- STEP 4 -->
            <div id="Step4" class="hidden">
                <div class="d-flex flex-column justify-content-center align-items-center">
                    <h1 style="color: #198754">Готово</h1>
                    <svg xmlns="http://www.w3.org/2000/svg" width="128" height="128" fill="currentColor" class="bi bi-check2" viewBox="0 0 16 16" style="color: #198754;">
                        <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z" />
                    </svg>
                    <div style="margin-top: 15px;">
                        <a href="web/index.php" class="btn btn-success">Перейти на сайт</a>

                    </div>
                </div>
            </div>
            <!-- STEP 5 -->
            <div id="Step5" class="hidden">
                <div class="d-flex flex-column justify-content-center align-items-center">
                    <h1 style="color: #dc3545">Ошибка</h1>
                    <p id="ErrorMessage" style="color: #dc3545">
                        Текст ошибки
                    </p>
                    <svg xmlns="http://www.w3.org/2000/svg" width="128" height="128" fill="currentColor" class="bi bi-x" viewBox="0 0 16 16" style="color: #dc3545;">
                        <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                    </svg>

                    <div style="margin-top: 15px;">
                        <a href="installer.php" class="btn btn-danger">Повторить</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="script.js"></script>
</body>

</html>