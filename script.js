
const containerStep1 = document.getElementById("Step1");
const containerStep2 = document.getElementById("Step2");
const containerStep3 = document.getElementById("Step3");
const containerStep4 = document.getElementById("Step4");
const containerStep5 = document.getElementById("Step5");
step(1);
var isTflex = 0;
var isAscon = 0;
/**
 * Проверка подключения к базе данных 
 * @param {string} idForm Идентификатор формы
 */
function checkConnect(idForm) {
    var form = document.querySelector("#" + idForm);
    var key = form.querySelector("input[name=key]");
    var host = form.querySelector("input[name=host]");
    var db = form.querySelector("input[name=db]");
    var username = form.querySelector("input[name=username]");
    var password = form.querySelector("input[name=password]");
    var url = "installer.php?key=" + key.value + "&host=" + host.value + "&db=" + db.value + "&username=" + username.value + "&password=" + password.value;
    var request = new XMLHttpRequest();
    request.open('GET', url);
    request.setRequestHeader('Content-Type', 'application/x-www-form-url');
    request.addEventListener("readystatechange", () => {
        if (request.readyState === 4 && request.status === 200) {
            var data;
            try {
                data = JSON.parse(request.responseText);
            } catch (err) {
                data = new Object()
                data.status = "error";
                data.message = err;
            }
            var status = form.querySelector(".status-text");
            if (data.status == 'success') {
                status.querySelector("span.status-text-text").innerHTML = "Соединение установлено";
                status.querySelector("input.form-check-input").checked = true;
                status.className = "status-text status-text-success";
                form.className = "form-check d-flex flex-column form-access";
            }
            else {
                status.querySelector("span.status-text-text").innerHTML = "Соединение не установлено";
                status.querySelector("input.form-check-input").checked = false;
                status.className = "status-text status-text-error";
                form.className = "form-check d-flex flex-column form-error";
                if (data.message != "") {
                    console.log(data.message);
                }
            }
        }
    });
    request.send();
}

/**
 * Сбросить данные формы
 * @param {string} idForm Идентификатор формы 
 */
function formReset(idForm) {
    var form = document.querySelector("#" + idForm);
    form.querySelectorAll("input").forEach(input => {
        input.removeAttribute("readonly");
        input.value = "";
    });
    var status = form.querySelector(".status-text");
    status.innerHTML = "Соединение не установлено";
    status.className = "status-text status-text-error";
}

/**
 * Запустить установку
 */
function next() {
    var formTflex = document.querySelector("#FormTflex");
    if (formTflex.querySelector("input.form-check-input").checked) {
        isTflex = 1;
    }
    var formTAscon = document.querySelector("#FormAscon");
    if (formTAscon.querySelector("input.form-check-input").checked) {
        isAscon = 1;
    }
    if (isTflex != 1 || isAscon != 1) {
        alert("Необходимо заполнить данные для подключения к базе данных T-Flex DOCs и ASCON Лоцман PLM и выполнить проверку");
        return;
    }
    step(2);
    var request = new XMLHttpRequest();
    request.open('GET', "installer.php?key=zip");
    request.setRequestHeader('Content-Type', 'application/x-www-form-url');
    request.addEventListener("readystatechange", () => {
        if (request.readyState === 4 && request.status === 200) {
            console.log(request.responseText);
            var resStep2;
            try {
                resStep2 = JSON.parse(request.responseText);
            } catch (err) {
                resStep2 = new Object()
                resStep2.status = "error";
                resStep2.message = err;
            }
            console.log(resStep2);
            if (resStep2.status == "success") {
                step(3);
                var requestStep3 = new XMLHttpRequest();
                requestStep3.open('GET', "installer.php?key=create_config_db");
                requestStep3.setRequestHeader('Content-Type', 'application/x-www-form-url');
                requestStep3.addEventListener("readystatechange", () => {
                    if (requestStep3.readyState === 4 && requestStep3.status === 200) {
                        console.log(requestStep3.responseText);
                        var resStep3;
                        try {
                            resStep3 = JSON.parse(requestStep3.responseText);
                        } catch (err) {
                            resStep3 = new Object()
                            resStep3.status = "error";
                            resStep3.message = err;
                        }
                        if (resStep3.status == "success") {
                            step(4);
                        } else {
                            console.log(resStep3.message);
                            step(5);
                        }
                    }
                });
                requestStep3.send();
            } else {
                console.log(resStep2.message);
                step(5);
            }
        }
    });
    request.send();
}

/**
 * Перейти на шаг
 * @param {*} n Номер шага
 */
function step(n = 1) {
    containerStep1.className = "hidden";
    containerStep2.className = "hidden";
    containerStep3.className = "hidden";
    containerStep4.className = "hidden";
    containerStep5.className = "hidden";
    if (n == 1) {
        containerStep1.className = "";
    }
    if (n == 2) {
        containerStep2.className = "";
    }
    if (n == 3) {
        containerStep3.className = "";
    }
    if (n == 4) {
        containerStep4.className = "";
    }
    if (n == 5) {
        containerStep5.className = "";
    }
}